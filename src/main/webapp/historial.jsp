<%-- 
    Document   : historial
    Created on : May 10, 2020, 1:12:26 PM
    Author     : Matico
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="root.model.entities.Consulta"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    List<Consulta> consultas = (List<Consulta>) request.getAttribute("consultas");
    Iterator<Consulta> itConsultas = consultas.iterator();
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Diccionario</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>
    <body class="m-0 bg-dark">
        <div class="container vh-100 bg-light">
            <nav class="row navbar navbar-expand-lg navbar-dark bg-dark justify-content-between" style="height: 10%">
                <span class="navbar-brand">Diccionario</span>

                <div class="navbar-nav">
                    <a class="nav-item nav-link" href="index.jsp">Buscar</a>

                    <form id="historialForm" action="controller">
                        <a class="nav-item nav-link" href="#" id="historialLink">Historial</a>
                    </form>
                </div>
            </nav>

            <div class="row justify-content-center bg-light overflow-auto" style="height: 70%">                
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Palabra</th>
                            <th scope="col">Definición</th>
                            <th scope="col">Fecha</th>
                        </tr>
                    </thead>
                    <tbody>
                        <% while (itConsultas.hasNext()) {
                                    Consulta con = itConsultas.next();%>
                        <tr>
                            <td><%= con.getConId()%></td>
                            <td><%= con.getConPalabra()%></td>
                            <td><%= con.getConDefinicion()%></td>
                            <td><%= con.getConFecha() %></td>
                        </tr>
                        <% }%>
                    </tbody>
                </table>
            </div>

            <div class="row d-flex flex-column justify-content-center" style="height: 10%; background-color: cadetblue">
                <div class="alert alert-light text-center" role="alert" style="margin: 1em 2em">
                    <a href="https://bitbucket.org/matiac/eval-final/src/master/" class="alert-link stretched-link" target="_blank">Código fuente en Bitbucket</a>
                </div>
            </div>

            <nav class="row navbar navbar-dark bg-dark" style="height: 10%">
                <span class="navbar-text">Matías Andrade Castro</span>
                <span class="navbar-text">|</span>
                <span class="navbar-text">Taller de Aplicaciones Empresariales</span>
                <span class="navbar-text">|</span>
                <span class="navbar-text">Sección 50</span>
            </nav>
        </div>

        <script>
            document.getElementById("historialLink").onclick = function() {
                document.getElementById("historialForm").submit();
            }
        </script>
    </body>
</html>
