<%-- 
    Document   : resultado
    Created on : May 10, 2020, 1:11:48 PM
    Author     : Matico
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    boolean éxito = (boolean) request.getAttribute("éxito");
    ArrayList<String> definiciones = (ArrayList<String>) request.getAttribute("definiciones");
    Iterator<String> itDefiniciones = definiciones.iterator();
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Diccionario</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>
    <body class="m-0 bg-dark">
        <div class="container vh-100 bg-light">
            <nav class="row navbar navbar-expand-lg navbar-dark bg-dark justify-content-between" style="height: 10%">
                <span class="navbar-brand">Diccionario</span>

                <div class="navbar-nav">
                    <a class="nav-item nav-link" href="index.jsp">Buscar</a>

                    <form id="historialForm" action="controller">
                        <a class="nav-item nav-link" href="#" id="historialLink">Historial</a>
                    </form>
                </div>
            </nav>
            
            <div class="row justify-content-center" style="height: 70%; background-color: cadetblue">
                <div class="col-sm-8 d-flex flex-column justify-content-center">   
                    <% if (éxito) { %>                                 
                    <h3 class="mb-3 text-light">Resultados:</h3>

                    <ul class="list-group list-group-flush border rounded p-4 bg-white">
                        <% while (itDefiniciones.hasNext()) {%>
                        <li class="list-group-item font-weight-light"><%= itDefiniciones.next()%></li>
                            <% } %>
                    </ul>
                    <% } else { %>
                    <div class="alert alert-danger text-center" role="alert">La palabra consultada no ha sido encontrada</div>
                    <% }%>
                </div>
            </div>

            <div class="row d-flex flex-column justify-content-center" style="height: 10%; background-color: cadetblue">
                <div class="alert alert-light text-center" role="alert" style="margin: 1em 2em">
                    <a href="https://bitbucket.org/matiac/eval-final/src/master/" class="alert-link stretched-link" target="_blank">Código fuente en Bitbucket</a>
                </div>
            </div>

            <nav class="row navbar navbar-dark bg-dark" style="height: 10%">
                <span class="navbar-text">Matías Andrade Castro</span>
                <span class="navbar-text">|</span>
                <span class="navbar-text">Taller de Aplicaciones Empresariales</span>
                <span class="navbar-text">|</span>
                <span class="navbar-text">Sección 50</span>
            </nav>
        </div>

        <script>
            document.getElementById("historialLink").onclick = function() {
                document.getElementById("historialForm").submit();
            }
        </script>
    </body>
</html>
