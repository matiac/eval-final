/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Matico
 */
@Entity
@Table(name = "dic_consultas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Consulta.findAll", query = "SELECT c FROM Consulta c"),
    @NamedQuery(name = "Consulta.findByConId", query = "SELECT c FROM Consulta c WHERE c.conId = :conId"),
    @NamedQuery(name = "Consulta.findByConPalabra", query = "SELECT c FROM Consulta c WHERE c.conPalabra = :conPalabra"),
    @NamedQuery(name = "Consulta.findByConDefinicion", query = "SELECT c FROM Consulta c WHERE c.conDefinicion = :conDefinicion"),
    @NamedQuery(name = "Consulta.findByConFecha", query = "SELECT c FROM Consulta c WHERE c.conFecha = :conFecha")})
public class Consulta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "con_id")
    private Long conId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "con_palabra")
    private String conPalabra;
    //@Array(databaseType="TEXT[]")
    @Size(max = 2147483647)
    @Column(name = "con_definicion")
    private String conDefinicion;
    @Column(name = "con_fecha", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date conFecha;

    public Consulta() {
    }

    public Consulta(Long conId) {
        this.conId = conId;
    }

    public Consulta(Long conId, String conPalabra) {
        this.conId = conId;
        this.conPalabra = conPalabra;
    }

    public Long getConId() {
        return conId;
    }

    public void setConId(Long conId) {
        this.conId = conId;
    }

    public String getConPalabra() {
        return conPalabra;
    }

    public void setConPalabra(String conPalabra) {
        this.conPalabra = conPalabra;
    }

    public Serializable getConDefinicion() {
        return conDefinicion;
    }

    public void setConDefinicion(String conDefinicion) {
        this.conDefinicion = conDefinicion;
    }

    public Date getConFecha() {
        return conFecha;
    }

    public void setConFecha(Date conFecha) {
        this.conFecha = conFecha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (conId != null ? conId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Consulta)) {
            return false;
        }
        Consulta other = (Consulta) object;
        if ((this.conId == null && other.conId != null) || (this.conId != null && !this.conId.equals(other.conId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.model.entities.Consulta[ conId=" + conId + " ]";
    }
    
}
