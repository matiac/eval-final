/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import root.model.dao.ConsultaDAO;
import root.model.entities.Consulta;

/**
 *
 * @author Matico
 */
@WebServlet(name = "Controller", urlPatterns = {"/controller"})
public class Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private final ConsultaDAO dao = new ConsultaDAO();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Controller</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Consulta> cons = dao.findConsultaEntities();
        request.setAttribute("consultas", cons);
        request.getRequestDispatcher("historial.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("¡HEY! (Controller)");

        Consulta con = new Consulta();
        con.setConPalabra(request.getParameter("palabra"));
        Response apiResponse = this.apiGet(request);
        ArrayList<String> definiciones = new ArrayList<>();

        if (apiResponse.getStatus() == 200) {
            System.out.println("200!");
            String output = apiResponse.readEntity(String.class);
            definiciones = this.parseJson(output);
            request.setAttribute("éxito", true);
        } else {
            System.out.println("Nop");
            definiciones.add("Palabra no encontrada");
            request.setAttribute("éxito", false);
        }

        request.setAttribute("definiciones", definiciones);
        con.setConDefinicion(definiciones.get(0));
        dao.create(con);

        request.getRequestDispatcher("resultado.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private Response apiGet(HttpServletRequest req) {
        String urlApi = this.getBaseUrl(req) + "/api/diccionario/";
        String palabra = req.getParameter("palabra").toLowerCase();
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(urlApi + palabra);
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        return invocationBuilder.get();
    }

    private String getBaseUrl(HttpServletRequest req) {
        StringBuffer url = req.getRequestURL();
        String uri = req.getRequestURI();
        String ctx = req.getContextPath();
        return url.substring(0, url.length() - uri.length() + ctx.length());
    }

    private ArrayList<String> parseJson(String json) {
        JSONObject obj = new JSONObject(json);
        ArrayList<String> definiciones = new ArrayList<>();

        JSONArray entries = obj.getJSONArray("results")
                .getJSONObject(0)
                .getJSONArray("lexicalEntries")
                .getJSONObject(0)
                .getJSONArray("entries");

        for (int i = 0; i < entries.length(); i++) {
            String definicion = entries.getJSONObject(i)
                    .getJSONArray("senses")
                    .getJSONObject(0)
                    .getJSONArray("definitions")
                    .getString(0);
            definiciones.add(definicion);
        }

        return definiciones;
    }
}
