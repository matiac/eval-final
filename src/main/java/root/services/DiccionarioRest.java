/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.services;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Matico
 */
@Path("/diccionario")
public class DiccionarioRest {

    @Context
    HttpServletRequest request;

    private final String oxfordApiUrl = "https://od-api.oxforddictionaries.com/api/v2/entries/es/";
    private final String oxfordApiParams = "?fields=definitions&strictMatch=false";
    private final String appId = "a3ecf551";
    private final String appKey = "524dff3e4582cc4cd549ff4ac676540d";

    @GET
    @Path("/{palabra}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscar(@PathParam("palabra") String palabra) {

        System.out.println("¡HEY! (API)... palabra: " + palabra);

        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(oxfordApiUrl + palabra + oxfordApiParams);
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        invocationBuilder.header("app_id", appId).header("app_key", appKey);
        return invocationBuilder.get();
    }
}
